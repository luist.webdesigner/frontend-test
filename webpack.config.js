const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: ["./src/front/App.jsx"],
    module: {
        rules: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    }
                ]
            }
        ]
    },
    devServer: {
        port:4000
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./src/front/index.html",
        })
    ]

};
