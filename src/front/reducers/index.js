import {TURN_LOADING} from "../constants/action-types";

export const initialState = {
    loaded: false
};
const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case TURN_LOADING:
            state.loaded = !state.loaded;
            console.log(state,action)
            return state;
        default:
            return state;
    }
}
export default rootReducer;

