import React, { Component } from "react";
import ReactDOM from "react-dom";
import Container from './components/Container.jsx'
const wrapper = document.getElementById("App");
import { Provider } from "react-redux";
import { store } from "./store/index";

function CommentsApp(props) {
    return <Container />
}

ReactDOM.render((
    <Provider store={store}>
        <CommentsApp />
    </Provider>
),wrapper);
