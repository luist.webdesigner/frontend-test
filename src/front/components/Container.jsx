import React, {Fragment, Component } from "react";
import {AppBar,Toolbar} from '@material-ui/core';
import Devices from '@material-ui/icons/Devices';
import Typography from '@material-ui/core/Typography';
import PhoneListContainer from './PhoneListContainer.jsx'
import ReactLoading from 'react-loading';
import { connect } from "react-redux";
import {turnLoading} from "../actions";

class Container extends Component {
    render() {
        return (
            <Fragment>
                <AppBar position="static" >
                    <Toolbar>
                        <Devices />
                        <Typography style={{paddingLeft:'8px'}} variant="h6" color="inherit" noWrap>
                            Phone Catalog
                        </Typography>
                    </Toolbar>
                </AppBar>
                <main>
                    {console.log(this.props.turnLoading)}
                    {!this.props.loaded && <ReactLoading onClick={this.props.turnLoading} type='spinningBubbles' color={'black'} height={50} width={50} />}
                        <PhoneListContainer/>
                </main>
            </Fragment>
        );
    }
}
const mapDispatchToProps = {
    turnLoading
};

const mapStateToProps = state => ({
    loaded: state.loaded
});
const Connector = connect(mapStateToProps,mapDispatchToProps)(Container);

export default Connector
