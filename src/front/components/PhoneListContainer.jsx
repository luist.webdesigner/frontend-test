import React, { Component } from "react";
import {Grid,Card,CardMedia,CardContent,CardActions,Button} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import CONSTANTS from '../CONSTANTS.json'
import { turnLoading } from "../actions/index";
import { connect } from "react-redux";

let phones;

class PhoneListContainer extends Component {

    constructor(props){
        super(props);
        this.state = {loaded:false}
        axios.get(CONSTANTS.server+"/phones").then((result)=>{
            phones = result.data;
            console.log("juas")
            this.props.turnLoading();
        })
    }


    render() {
        console.log("loading")
        if(this.props.loaded){
            return (
                <Grid container style={{padding:'20px'}} spacing={40}>
                    {phones.map(phone => (
                        <Grid item key={phone.id} sm={6} md={4} lg={3}>
                            <Card>
                                <CardMedia style={{height:'250px'}}
                                           image={CONSTANTS.server+phone.photoUrl} // eslint-disable-line max-len
                                           title={phone.title}
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {phone.title}
                                    </Typography>
                                    <Typography>
                                        {phone.description}
                                    </Typography>
                                    <Typography style={{marginTop:'10px'}} variant="h5" component="h2">
                                        {phone.price} €
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Button size="small" color="primary">
                                        View More
                                    </Button>

                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>

            );
        }else {
            return (<div></div>)
        }



    }
}
const mapDispatchToProps = {
    turnLoading
};

const mapStateToProps = state => ({
    loaded: state.loaded
});

const ConnectedPhoneListContainer = connect(mapStateToProps,mapDispatchToProps)(PhoneListContainer);


export default ConnectedPhoneListContainer;
