import { createStore } from "redux";
import rootReducer,{initialState} from "../reducers";

export const reducer = {rootReducer}

export const store = createStore(rootReducer, initialState);
